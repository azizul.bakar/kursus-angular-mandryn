'use strict';

describe('Controller: Raman2Ctrl', function () {

  // load the controller's module
  beforeEach(module('spmbtestApp'));

  var Raman2Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    Raman2Ctrl = $controller('Raman2Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(Raman2Ctrl.awesomeThings.length).toBe(3);
  });
});
