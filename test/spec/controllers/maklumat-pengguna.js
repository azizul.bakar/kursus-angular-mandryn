'use strict';

describe('Controller: MaklumatPenggunaCtrl', function () {

  // load the controller's module
  beforeEach(module('spmbtestApp'));

  var MaklumatPenggunaCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MaklumatPenggunaCtrl = $controller('MaklumatPenggunaCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MaklumatPenggunaCtrl.awesomeThings.length).toBe(3);
  });
});
