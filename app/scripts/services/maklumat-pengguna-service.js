'use strict';

/**
 * @ngdoc service
 * @name spmbtestApp.maklumatPenggunaService
 * @description
 * # maklumatPenggunaService
 * Factory in the spmbtestApp.
 */
angular.module('spmbtestApp')
  .factory('maklumatPenggunaService', ['$http', function ($http) {

    // Public API here
    return {
      daftarPengguna: function (paramObj) {
        return $http.post('./api/training/insert-api.php', paramObj);
      },
      SenaraiPengguna: function (paramObj) {
        return $http.get('./api/training/select-api.php', paramObj);
      }
    };
  }]);
