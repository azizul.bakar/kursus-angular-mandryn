'use strict';

/**
 * @ngdoc function
 * @name spmbtestApp.controller:MaklumatPenggunaCtrl
 * @description
 * # MaklumatPenggunaCtrl
 * Controller of the spmbtestApp
 */
angular.module('spmbtestApp')
  .controller('MaklumatPenggunaCtrl', ['$log', 'maklumatPenggunaService', '$uibModal',
    function ($log, PenggunaService, $uibModal) {

      var $ctrl = this;

      $ctrl.pengguna = {};
      $ctrl.senaraiPengguna = [
        { "nama": "Abu Seman", "emel": "abu.seman@jpa.gov.my" },
        { "nama": "Ahmad Zain", "emel": "ahmad.zain@jpa.gov.my" }
      ];

      $ctrl.senaraiGred = [];

      $ctrl.senaraiSkim = [
        { "kod": "0A01", "perihal": "JURUTERA LAUT" },
        { "kod": "0A02", "perihal": "JURUTERBANG \/ PEMERIKSA JURUTERBANG" },
        { "kod": "0A03", "perihal": "MALIM" }
      ];

      $ctrl.addPengguna = function () {

        $log.info('data add pengguna', $ctrl.pengguna);

        PenggunaService.daftarPengguna($ctrl.pengguna).then(function (resp) {

          $log.info('data resp.data', resp.data);

          $ctrl.message = resp.data;

        }, function (err) {

          $log.error('error addPengguna', err);

        });

      };

      $ctrl.getAllPengguna = function () {

        PenggunaService.SenaraiPengguna().then(function (resp) {

          $log.info('data getAllPengguna resp.data', resp.data);
          $ctrl.senaraiPengguna = resp.data;

        }, function (err) {
          $log.error('error getAllPengguna', err);
        });

      };

      $ctrl.getAllPengguna();


      $ctrl.bindGred = function () {

        if ($ctrl.pengguna.skim != null) {

          $ctrl.senaraiGred = [
            { "gred_kod": "0A01A54", "gred_perihal": "A54" },
            { "gred_kod": "0A01A52", "gred_perihal": "A52" }
          ];
          $ctrl.pengguna.gred = '';

        } else {

          $ctrl.senaraiGred = [];
        }

      };

      //$ctrl.bindGred();

      $ctrl.open = function () {

        var modalInstance = $uibModal.open({
          animation: true,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: 'myModalContent.html',
          controller: 'ModalInstanceCtrl',
          controllerAs: '$ctrl',
          size: 'lg',
          resolve: {
            items: function () {
              return $ctrl.senaraiSkim;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          $ctrl.selected = selectedItem;
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });

      };

      $ctrl.open();

    }]);
