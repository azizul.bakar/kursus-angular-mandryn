'use strict';

/**
 * @ngdoc function
 * @name spmbtestApp.controller:Raman2Ctrl
 * @description
 * # Raman2Ctrl
 * Controller of the spmbtestApp
 */
angular.module('spmbtestApp')
  .controller('Raman2Ctrl', function () {
    var $ctrl = this;

    $ctrl.title = "my view";

    $ctrl.student = {
      "nokp": "78542525558",
      "nama": "bob raman",
      "jantina": "L"
    };

    $ctrl.students = [
      {
        "nokp": "78542525558",
        "nama": "bob raman",
        "jantina": "L"
      },
      {
        "nokp": "923344566558",
        "nama": "Wahidah Jjul",
        "jantina": "P"
      },
      {
        "nokp": "88542525558",
        "nama": "Dil Watson",
        "jantina": "P"
      }
    ];

  });
