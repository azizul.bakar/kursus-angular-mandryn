'use strict';

/**
 * @ngdoc overview
 * @name spmbtestApp
 * @description
 * # spmbtestApp
 *
 * Main module of the application.
 */
angular
  .module('spmbtestApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider,$locationProvider) {

    $locationProvider.hashPrefix('');

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html'
      })
      .when('/about', {
        templateUrl: 'views/about.html'
      })
      .when('/raman', {
        templateUrl: 'views/raman.html'
      })
      .when('/raman2', {
        templateUrl: 'views/raman2.html'
      })
      .when('/maklumat-pengguna', {
        templateUrl: 'views/maklumat-pengguna.html'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
